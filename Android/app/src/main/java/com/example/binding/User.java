package com.example.binding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.Date;

/**
 * Created by emarc.magtanong on 7/6/15.
 */
public class User extends BaseObservable {
    private String id;
    private String userName;
    private Boolean passwordVisibility;
    private Boolean capitalize = false;
    private Date lastAccess;

    public User(String id, String userName, boolean passwordVisibility, Date lastAccess) {
        this.id = id;
        this.userName = userName;
        this.passwordVisibility = passwordVisibility;
        this.lastAccess = lastAccess;
    }

    @Bindable
    public String getId() {
        return this.id;
    }

    @Bindable
    public String getLastName() {
        return this.userName;
    }

    @Bindable
    public boolean getPasswordVisibility() {
        return this.passwordVisibility;
    }

    @Bindable
    public Boolean getCapitalize() {
        return capitalize;
    }

    @Bindable
    public Date getLastAccess() {
        return lastAccess;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public void setLastName(String lastName) {
        this.userName = lastName;
        notifyPropertyChanged(BR.passwordVisibility);
    }

    public void setPasswordVisibility(boolean isAdult) {
        this.passwordVisibility = isAdult;
        notifyPropertyChanged(BR.passwordVisibility);
    }

    public void setCapitalize(Boolean capitalize) {
        this.capitalize = capitalize;
        notifyPropertyChanged(BR.capitalize);
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
        notifyPropertyChanged(BR.lastAccess);
    }
}
