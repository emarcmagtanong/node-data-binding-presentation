package com.example.binding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by emarc.magtanong on 7/6/15.
 */
public class MyStringUtils {
    public static String capitalize(String string) {
        return string.toUpperCase();
    }

    public static String toLower(String string) {
        return string.toLowerCase();
    }

    public static String generateIdNumber() {
        int id = new Random(System.currentTimeMillis()).nextInt(Integer.MAX_VALUE) + 1;
        return id + "";
    }

    public static String dateToHumanDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(date);
    }
}
