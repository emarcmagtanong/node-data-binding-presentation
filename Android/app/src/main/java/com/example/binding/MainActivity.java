package com.example.binding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.binding.databinding.ActivityMainBinding;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Calendar calendar = Calendar.getInstance();
        mUser = new User(MyStringUtils.generateIdNumber(), "emarc.magtanong", true, calendar.getTime());
        binding.setUser(mUser);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.change_id_number)
    void changeIdNumber() {
        mUser.setId(MyStringUtils.generateIdNumber());
    }

    @OnClick(R.id.capitalize_toggle)
    void capitalizeToggleButton() {
        mUser.setCapitalize(!mUser.getCapitalize());
    }

    @OnClick(R.id.change_access)
    void changeAccessButton() {
        Calendar calendar = Calendar.getInstance();
        mUser.setLastAccess(calendar.getTime());
    }

    @OnClick(R.id.visibility_toggle)
    void adultToggleButton() {
        mUser.setPasswordVisibility(!mUser.getPasswordVisibility());
    }
}
